import kivy
import lbry_api
kivy.require('2.0.0')
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from lbry_ui import RootTabPanel, SearchTab, UploadTab, UploadStatusTab


class BookNetApp(App):
    root_layout = None
    tab_panel = None
    search_tab = None
    upload_tab = None
    upload_status_tab = None
    upload_status_tab_header = None
    status_label = None
    lbry_api_online = False
    lbry_api = lbry_api.LbryAPI()

    def lbry_api_online_callback(self):
        Clock.schedule_once(self.upload_tab.api_online_callback, 0)

    def check_lbry_api_status(self, dt=None):
        # check status of LBRY API
        self.lbry_api_online = self.lbry_api.check_lbry_status()
        self.update_status_label()

        if self.lbry_api_online:
            self.lbry_api_online_callback()

    def update_status_label(self):
        # when check_lbry_status completed, update label text for status line
        lbry_status_str = "online" if self.lbry_api_online else "down"
        status_line_str = f'[status {lbry_status_str}]'
        self.status_label.text = status_line_str

    def file_upload_callback(self, results):
        self.upload_status_tab.add_to_uploads_list(results)
        self.tab_panel.switch_to(self.upload_status_tab_header, do_scroll=True)

    def build(self):
        Clock.schedule_once(self.check_lbry_api_status, 1)

        self.root_layout = FloatLayout()

        self.search_tab = SearchTab()
        self.upload_tab = UploadTab(file_upload_callback=self.file_upload_callback)
        self.upload_status_tab = UploadStatusTab()

        # create tab panel and add tabs
        self.tab_panel = RootTabPanel(size_hint=(0.95, 0.95),
                                      pos_hint={'top': 1.0, 'center_x': 0.5})

        self.tab_panel.add_tab(title='Search', content_layout=self.search_tab.layout)
        self.tab_panel.add_tab(title='Upload', content_layout=self.upload_tab.layout)
        self.upload_status_tab_header = self.tab_panel.add_tab(title="Upload Status", content_layout=self.upload_status_tab.layout)

        self.root_layout.add_widget(self.tab_panel)

        # ------------ misc ------------

        self.status_label = Label(text="", halign='center', valign='middle', text_size=(100.0, 50.0), pos_hint={'x': 0.0, 'y': -0.48})
        self.root_layout.add_widget(self.status_label)

        self.update_status_label()
        return self.root_layout


if __name__ == '__main__':
    BookNetApp().run()