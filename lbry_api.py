import requests
import re

LBRY_DAEMON_URL = "http://localhost:5279"

class LbryAPI():
    def __init__(self):
        super().__init__()

    def check_lbry_status(self):
        # async call to check_lbry_status
        status_args_dict = {"method": "status", "params": {}}
        try:
            response = requests.post(LBRY_DAEMON_URL, json=status_args_dict)
        except requests.exceptions.RequestException:
            status_bool = False
        else:
            if response.status_code == 200:
                status_bool = True
            else:
                status_bool = False

        return status_bool

    def fetch_channel_list(self):
        channel_list_params = {
            "method": "channel_list",
            "params": {},
        }

        response = requests.post(LBRY_DAEMON_URL, json=channel_list_params)
        response_json = response.json()
        print(response_json)

        output_channel_list = []
        for channel_dict in response_json['result']['items']:
            output_channel_list.append({
                'name': channel_dict['name'],
                'id': channel_dict['claim_id'],
            })

        return output_channel_list

    def search_lbry(self, search_text, file_format=None, genre=None, booknet_only=None, order_by=None):
        search_params = {
            'text': search_text,
            # 'name': search_text,
            'all_tags': [],
        }

        if file_format:
            search_params['all_tags'].append(f'format:{file_format}')

        if genre:
            search_params['all_tags'].append(f'genre:{genre}')

        if booknet_only:
            search_params['all_tags'].append('booknet')

        if not order_by:
            search_params['order_by'] = ['amount', '^name']

        print(f"search_params: {repr(search_params)}")

        status_args_dict = {"method": "claim_search", "params": search_params}
        response = requests.post(LBRY_DAEMON_URL, json=status_args_dict)
        result_obj = response.json()
        results_list = []

        for result_item in result_obj['result']['items']:
            result_val = result_item['value']
            try:
                channel_name = result_item['signing_channel']['name']
            except KeyError:
                channel_name = "-"

            try:
                fee_dict = result_val['fee']
                fee_amount = fee_dict['amount']
            except KeyError:
                fee_amount = "0.0"

            try:
                thumbnail_dict = result_val['thumbnail']
                thumbnail_url = thumbnail_dict['url']
            except KeyError:
                thumbnail_url = None

            item_dict = {
                'author': result_val.get('author', channel_name),
                'title': result_val.get('title'),
                'price': f"{fee_amount} LBC",
                'amount': result_item['amount'],
                'thumbnail': thumbnail_url,
                'tags': result_val.get('tags', []),
                'url': result_item['canonical_url'],
            }
            results_list.append(item_dict)

        return results_list

    def upload_to_lbry(self, title, file_path, bid, fee=None, author=None, format=None, genre=None, tag_list=None, channel_id=None):
        print(f"upload_to_lbry: {title=} {file_path=}")

        clean_title_regex = re.compile('([^a-zA-Z0-9\-]*)')

        clean_name = title.strip().replace(" ", "-")
        clean_name = clean_title_regex.sub("", clean_name)

        if tag_list is None:
            tag_list = []

        if format:
            format_tag = f"format:{format}"
            tag_list.append(format_tag)

        if genre:
            genre_tag = f"genre:{genre}"
            tag_list.append(genre_tag)

        # always add the booknet tag
        tag_list.append("booknet")

        name = f"{clean_name}"
        upload_args_dict = {
            "method": "publish",
            "params": {
                "name": name,
                "title": title,
                "file_path": file_path,
                "author": author,
                "tags": tag_list,
                "bid": bid,
                "fee_amount": fee,
                "fee_currency": "LBC",
                "preview": False,
            }
        }

        if channel_id:
            upload_args_dict['params']["channel_id"] = channel_id

        print(f"{upload_args_dict=}")

        response = requests.post(LBRY_DAEMON_URL, json=upload_args_dict)
        print(response)
        print(response.json())

        response_json = response.json()

        upload_name = None
        for tx_output in response_json['result']['outputs']:
            if 'name' in tx_output:
                upload_name = tx_output['name']
                break

        return {
            'txid': response_json['result']['txid'],
            'name': upload_name,
        }

    def fetch_tx_status(self, txid):
        transaction_show_args_dict = {"method": "transaction_show", "params": {
            'txid': txid
        }}
        response = requests.post(LBRY_DAEMON_URL, json=transaction_show_args_dict)

        response_json = response.json()
        print(response_json)

        confirmation_count = None
        for tx_output in response_json['result']['outputs']:
            if 'confirmations' in tx_output:
                confirmation_count = tx_output['confirmations']
                break

        return {
            'height': response_json['result']['height'],
            'confirmations': confirmation_count,
        }

    def download_file(self, file_uri, callback_func=None):
        get_args_dict = {
            "method": "get",
            "params": {
                "uri": file_uri,
                "save_file": True
            }
        }
        response = requests.post(LBRY_DAEMON_URL, json=get_args_dict)
        print(response)
        print(response.json())
        if callback_func:
            callback_func(response_json=response.json())
        return response.json()
