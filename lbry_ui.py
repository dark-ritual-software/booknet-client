from collections import OrderedDict
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.filechooser import FileChooser, FileChooserListView
from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from kivy.uix.spinner import Spinner
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelHeader
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton
import functools
import lbry_api
import webbrowser

FORMAT_VALUE_CHOICES = (
    'epub', 'mobi', 'pdf', 'txt'
)
NO_FORMAT_VALUE = "Any File Format"

GENRE_VALUE_CHOICES = (
    'sci-fi', 'fantasy', 'historical fiction', 'aa', 'the quick brown fox jumps over the lazy dog'
)
NO_GENRE_VALUE = "Any Genre"


class RootTabPanel(TabbedPanel):
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, do_default_tab=False, **kwargs)

    def add_tab(self, title, content_layout):
        tab_header = TabbedPanelHeader(text=title)
        tab_header.content = content_layout
        self.add_widget(tab_header)
        return tab_header


class Tab(object):
    def __init__(self):
        self.lbry_api = lbry_api.LbryAPI()
        self.layout = BoxLayout(orientation='vertical', spacing=5)


class UploadStatusTab(Tab):
    uploads_list = []

    def __init__(self):
        super().__init__()

        self.upload_status_list = BoxLayout(orientation='vertical', size_hint_x=1.0, size_hint_y=None, spacing=20)
        self.upload_status_view = ScrollView(size_hint=(1.0, 1.0), do_scroll_x=False, do_scroll_y=True)

        self.upload_status_view.add_widget(self.upload_status_list)
        self.layout.add_widget(self.upload_status_view)

    def add_to_uploads_list(self, upload_results_dict):
        def open_link_in_browser(instance, value):
            webbrowser.open_new_tab(value)

        print("add_to_uploads_list called ", upload_results_dict)
        self.uploads_list.append(upload_results_dict)

        upload_result_container = BoxLayout(orientation='vertical')
        upload_result_container_line1 = BoxLayout(orientation='horizontal')
        upload_result_container_line2 = BoxLayout(orientation='horizontal')
        upload_result_container_line3 = BoxLayout(orientation='horizontal')
        upload_result_container.add_widget(upload_result_container_line1)
        upload_result_container.add_widget(upload_result_container_line2)
        upload_result_container.add_widget(upload_result_container_line3)

        upload_name = upload_results_dict['name']
        txid = upload_results_dict['txid']

        upload_name_label = Label(text=upload_name)
        upload_result_container_line1.add_widget(upload_name_label)

        def update_upload_status_label(dt, txid, status_label):
            status_dict = self.lbry_api.fetch_tx_status(txid)

            tx_stage_text = ""
            if status_dict['height'] == -2:
                tx_stage_text = "Waiting for submission"
            elif status_dict['height'] == -1:
                tx_stage_text = "Waiting for validation"
            elif status_dict['height'] == 0:
                tx_stage_text = "Validated"
            else:
                tx_stage_text = "Published to blockchain"

            confirmation_count = status_dict['confirmations']

            confirmation_text = f"{confirmation_count}"
            if confirmation_count >= 5:
                confirmation_text = "5+"

            status_label.text = f"status: {tx_stage_text}   confirmations: {confirmation_text}"

            if confirmation_count < 5:
                update_upload_status_partial = functools.partial(update_upload_status_label, txid=txid,
                                                                 status_label=upload_status_label)
                Clock.schedule_once(update_upload_status_partial, 5)

        upload_status_label = Label(text="...")
        upload_result_container_line2.add_widget(upload_status_label)

        update_upload_status_partial = functools.partial(update_upload_status_label, txid=txid, status_label=upload_status_label)
        Clock.schedule_once(update_upload_status_partial, 5)

        explorer_url = f"https://explorer.lbry.com/tx/{txid}"
        explorer_link = Label(text=f"[ref={explorer_url}]Open in LBRY Explorer[/ref]", markup=True, size_hint=(0.3, 1.0))
        explorer_link.bind(on_ref_press=open_link_in_browser)
        txid_label = Label(text=txid, size_hint=(0.7, 1.0))

        upload_result_container_line3.add_widget(txid_label)
        upload_result_container_line3.add_widget(explorer_link)
        self.upload_status_list.add_widget(upload_result_container)


class UploadTab(Tab):
    DEFAULT_BID = "0.02"
    DEFAULT_FEE = "0.0"
    active_channel_name = 'Anonymous'
    selected_file_path = None
    channel_lookup = {
        'Anonymous': None
    }
    channel_list = []

    def __init__(self, file_upload_callback):
        super().__init__()

        self.file_upload_callback = file_upload_callback

        booknet_label = Label(text='LBRY BookNet')
        self.layout.add_widget(booknet_label)

        self.channel_field_input = Spinner(values=[self.active_channel_name], text=self.active_channel_name,
                                           text_autoupdate=True)
        self.add_form_field_with_label(self.channel_field_input, 'Channel')

        def channel_field_changed(spinner_widget, selected_channel_name):
            self.active_channel_name = selected_channel_name
            print("active channel changed", self.active_channel_name, self.channel_lookup[self.active_channel_name])

        self.channel_field_input.bind(text=channel_field_changed)

        self.file_field_btn = Button(text='Choose File', font_size=14)
        self.add_form_field_with_label(self.file_field_btn, 'File')

        self.file_field_btn.bind(on_release=self.open_filechooser_popup)

        self.title_field_input = TextInput(text="")
        self.add_form_field_with_label(self.title_field_input, 'Title')

        self.format_field_input = Spinner(values=("",) + FORMAT_VALUE_CHOICES, text_autoupdate=True)
        self.add_form_field_with_label(self.format_field_input, 'Format')

        self.author_field_input = TextInput(text="")
        self.add_form_field_with_label(self.author_field_input, 'Author')

        self.genre_field_input = Spinner(values=("",) + GENRE_VALUE_CHOICES, text_autoupdate=True)
        self.add_form_field_with_label(self.genre_field_input, 'Genre')

        self.othertags_field_input = TextInput(text="")
        self.add_form_field_with_label(self.othertags_field_input, 'Other Tags')

        self.bid_field_input = TextInput(text=self.DEFAULT_BID)
        self.add_form_field_with_label(self.bid_field_input, 'Bid')

        self.fee_field_input = TextInput(text=self.DEFAULT_FEE)
        self.add_form_field_with_label(self.fee_field_input, 'Fee Amount')

        submit_btn = Button(text="Submit to LBRY", font_size=14)
        submit_btn.bind(on_release=self.save_pressed)
        self.layout.add_widget(submit_btn)

        self.clear_upload_form()

    def add_form_field_with_label(self, widget, label_text):
        field_layout = BoxLayout(orientation='horizontal')
        field_label = Label(text=label_text)
        field_layout.add_widget(field_label)
        field_layout.add_widget(widget)
        self.layout.add_widget(field_layout)

    def file_chosen_callback(self, chooser_widget, selection, touch_event, chooser_btn, popup_widget):
        print(self, "file_chosen_callback", repr(selection))
        self.selected_file_path = selection and selection[0]
        if self.selected_file_path:
            print("found ", self.selected_file_path)
            chooser_btn.text = self.selected_file_path
            popup_widget.dismiss()

        print("chose ", selection)

    def open_filechooser_popup(self, chooser_btn):
        file_chooser_widget = FileChooserListView(path="/", multiselect=False)
        filepicker_popup = Popup(title='Double-Click to Choose File', content=file_chooser_widget, size_hint=(0.8, 0.8))

        bound_file_chosen_callback = functools.partial(self.file_chosen_callback, chooser_btn=chooser_btn, popup_widget=filepicker_popup)
        print("open_filechooser_popup", bound_file_chosen_callback)
        file_chooser_widget.bind(on_submit=bound_file_chosen_callback)
        filepicker_popup.open()

    def clear_upload_form(self):
        self.bid_field_input.text = self.DEFAULT_BID
        self.fee_field_input.text = self.DEFAULT_FEE
        self.title_field_input.text = ""
        self.format_field_input.text = ""
        self.author_field_input.text = ""
        self.genre_field_input.text = ""
        self.othertags_field_input.text = ""
        self.file_field_btn.text = "Choose File"
        self.selected_file_path = None

    def save_pressed(self, btn_instance):
        obj_data = {
            "bid": self.bid_field_input.text,
            "fee": self.fee_field_input.text,
            "file": self.selected_file_path,
            "title": self.title_field_input.text,
            "format": self.format_field_input.text,
            "author": self.author_field_input.text,
            "genre": self.genre_field_input.text,
            "tags": self.othertags_field_input.text,
        }
        print("save pressed: obj_data = ", repr(obj_data))

        tag_list = [tag.strip() for tag in obj_data['tags'].split(",") if tag]

        results = self.lbry_api.upload_to_lbry(
            channel_id=self.channel_lookup[self.active_channel_name],
            bid=obj_data['bid'],
            fee=obj_data['fee'],
            title=obj_data['title'],
            file_path=obj_data['file'],
            author=obj_data['author'],
            format=obj_data['format'],
            genre=obj_data['genre'],
            tag_list=tag_list
        )
        self.file_upload_callback(results)
        self.clear_upload_form()

    def fetch_channel_list(self, dt=None):
        # fetch list of accounts from API
        self.channel_list = self.lbry_api.fetch_channel_list()
        for channel_dict in self.channel_list:
            self.channel_lookup[channel_dict['name']] = channel_dict['id']
        print("channel list fetched")

        # refresh channel choices spinner
        channel_field_options = sorted(self.channel_lookup.keys())
        self.channel_field_input.values = channel_field_options

    def api_online_callback(self, dt):
        Clock.schedule_once(self.fetch_channel_list, 0)


class SearchTab(Tab):
    def __init__(self):
        super().__init__()

        search_bar_layout = BoxLayout(orientation='vertical', spacing=5, size_hint=(1.0, 0.2))
        search_bar_top_line_layout = BoxLayout(orientation='horizontal',
                                               spacing=5, size_hint=(1.0, 0.7))
        search_bar_bottom_line_layout = BoxLayout(orientation='horizontal',
                                                  spacing=5, size_hint=(1.0, 0.3))

        self.search_input = TextInput(text="", multiline=False, size_hint=(0.8, 1.0))

        self.search_format_input = Spinner(values=(NO_FORMAT_VALUE,) + FORMAT_VALUE_CHOICES,
                                           text_autoupdate=True)
        self.search_genre_input = Spinner(values=(NO_GENRE_VALUE,) + GENRE_VALUE_CHOICES,
                                          text_autoupdate=True)
        self.search_button = Button(text="Search", size_hint=(0.2, 1.0))
        self.search_booknet_only_input = ToggleButton(text="BookNet Only", state='down')

        search_bar_top_line_layout.add_widget(self.search_input)
        search_bar_top_line_layout.add_widget(self.search_button)
        search_bar_bottom_line_layout.add_widget(self.search_format_input)
        search_bar_bottom_line_layout.add_widget(self.search_genre_input)
        search_bar_bottom_line_layout.add_widget(self.search_booknet_only_input)

        search_bar_layout.add_widget(search_bar_top_line_layout)
        search_bar_layout.add_widget(search_bar_bottom_line_layout)

        self.layout.add_widget(search_bar_layout)

        self.results_list = BoxLayout(orientation='vertical', size_hint_x=1.0, size_hint_y=None, spacing=20)
        self.results_view = ScrollView(size_hint=(1.0, 1.0), do_scroll_x=False, do_scroll_y=True)

        self.results_view.add_widget(self.results_list)
        self.layout.add_widget(self.results_view)

        self.search_input.fbind('on_text_validate', self.handle_search_input)
        self.search_button.fbind('on_release', self.handle_search_input)

    def handle_search_input(self, instance):
        search_str = self.search_input.text
        print(f"enter pressed on search {search_str=}")

        search_kwargs = {}
        if self.search_format_input.text != NO_FORMAT_VALUE:
            # format was picked
            search_kwargs['file_format'] = self.search_format_input.text

        if self.search_genre_input.text != NO_GENRE_VALUE:
            # genre was picked
            search_kwargs['genre'] = self.search_genre_input.text

        if self.search_booknet_only_input.state == 'down':
            search_kwargs['booknet_only'] = True

        search_results = self.lbry_api.search_lbry(search_str, **search_kwargs)
        self.results_list.clear_widgets()
        self.results_list.height = 0
        self.results_view.scroll_y = 1
        for search_result in search_results:
            self.add_search_result_to_layout(search_result)

    def add_search_result_to_layout(self, result_dict):
        result_text = result_dict['title'] or ''
        result_author = result_dict['author']
        result_tags_raw = result_dict['tags']
        result_tags = ", ".join(result_tags_raw)
        result_url = result_dict['url']
        result_price = result_dict['price']
        result_thumbnail = result_dict['thumbnail']

        search_result_dict = OrderedDict((
            ('text', result_text),
            ('author', result_author),
            ('price', result_price),
            ('tags', result_tags),
            ('url', result_url),
        ))

        search_result_layout = BoxLayout(orientation='vertical', size_hint=(1.0, None), height=150, spacing=5)

        search_result_line1 = BoxLayout(orientation='horizontal', size_hint=(1.0, None), height=50, spacing=5)
        search_result_line2 = BoxLayout(orientation='horizontal', size_hint=(1.0, None), height=50, spacing=5)
        search_result_line3 = BoxLayout(orientation='horizontal', size_hint=(1.0, None), height=50, spacing=5)

        title_label = Label(text=result_text, font_size=20, size_hint=(0.7, 1.0), pos_hint={'right': 1.0}, halign='left')
        author_label = Label(text=result_author, font_size=20, size_hint=(0.3, 1.0), halign='right')
        url_label = Label(text=result_url, font_size=16, size_hint=(0.6, 1.0), halign='left')
        price_label = Label(text=f"Price: {result_price}", font_size=16, size_hint=(0.2, 1.0), pos_hint={'right': 1.0}, halign='right')
        details_btn = Button(text="Show Details", font_size=16, size_hint=(0.2, 1.0), pos_hint={'right': 1.0}, halign='right')
        tags_label = Label(text=result_tags, font_size=16, size_hint=(1.0, 1.0), halign='left')

        bound_showdetails_func = functools.partial(self.show_details, search_result_dict=search_result_dict)
        details_btn.bind(on_release=bound_showdetails_func)

        def size_changed(layout, size):
            title_label.text_size = ((size[0] * title_label.size_hint_x) - 25, None)
            author_label.text_size = ((size[0] * author_label.size_hint_x) - 25, None)
            url_label.text_size = ((size[0] * url_label.size_hint_x) - 25, None)

        self.results_list.bind(size=size_changed)

        separator_label = Label(text="-"*200)

        if result_thumbnail:
            thumbnail_line = BoxLayout(orientation='horizontal', size_hint=(1.0, None), height=150, spacing=5)
            search_result_layout.height += 150
            thumbnail_image = AsyncImage(source=result_thumbnail)
            thumbnail_line.add_widget(thumbnail_image)

        search_result_line1.add_widget(title_label)
        search_result_line1.add_widget(author_label)
        search_result_line2.add_widget(url_label)
        search_result_line2.add_widget(price_label)
        search_result_line2.add_widget(details_btn)
        search_result_line3.add_widget(tags_label)

        search_result_layout.add_widget(search_result_line1)
        search_result_layout.add_widget(search_result_line2)
        search_result_layout.add_widget(search_result_line3)
        if result_thumbnail:
            search_result_layout.add_widget(thumbnail_line)
        search_result_layout.add_widget(separator_label)

        self.results_list.add_widget(search_result_layout)
        self.results_list.height += search_result_layout.height + self.results_list.spacing

    def show_details(self, button, search_result_dict):
        print(f"{button=} {search_result_dict=}")
        details_layout = BoxLayout(orientation='vertical')
        for display_name, display_value in search_result_dict.items():
            display_label = Label(text=f"{display_name.title()}: {display_value}")
            details_layout.add_widget(display_label)

        download_btn = Button(text="Download File")

        download_file_func = functools.partial(self.download_file, url=search_result_dict['url'])
        download_btn.bind(on_release=download_file_func)
        details_layout.add_widget(download_btn)

        details_popup = Popup(title=f'{search_result_dict["text"]} Details', content=details_layout, size_hint=(0.8, 0.8))
        details_popup.open()

    def download_progress_update(self, download_btn, url, response_json):
        print(response_json)
        result_json = response_json['result']
        if 'error' in result_json:
            download_txt = f"ERROR: {result_json['error']}"
        elif not result_json['completed']:
            written_bytes = result_json['written_bytes']
            total_bytes = result_json['total_bytes']
            download_percentage = (written_bytes / total_bytes) * 100.0
            download_txt = f"Downloading... {download_percentage:0.2f}%"
            callback_partial = functools.partial(self.download_progress_update, download_btn=download_btn, url=url)
            Clock.schedule_once(lambda dt: self.lbry_api.download_file(url, callback_func=callback_partial), 1)
        else:
            download_txt = "Download Complete!"

        download_btn.text = download_txt

    def download_file(self, download_btn, url):
        print("download")
        download_btn.disabled = True
        download_btn.text = "Requesting Download..."
        callback_partial = functools.partial(self.download_progress_update, download_btn=download_btn, url=url)
        Clock.schedule_once(lambda dt: self.lbry_api.download_file(url, callback_func=callback_partial), 0)
